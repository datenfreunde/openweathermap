#! /bin/env python
import sys
import click
import paho.mqtt.client as mqtt
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))


@click.command()
@click.option('--host',default='localhost',help='Host to connect to')
@click.option('--subscribe', default='#', help='Topics to subscribe')
def run(host,subscribe) :
    c=mqtt.Client()
    c.on_message=on_message
    c.connect(host)
    print("Listening to {}".format(host))
    c.subscribe(subscribe)
    print("Subscribed to {}".format(subscribe))
    c.loop_forever()


if __name__=="__main__" :
    run()
