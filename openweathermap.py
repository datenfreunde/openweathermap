import json
import logging
import sys
import click
import time
import requests
import os
import copy
import jmespath
from functools import lru_cache
from jmespathtemplate import jmespathtemplate
from collections import OrderedDict
import csv
import urllib
import paho.mqtt.client


logger=logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO,stream=sys.stderr,
         format="%(asctime)-15s %(name)s %(levelname)s %(filename)s:%(lineno)s %(message)s")

logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.CRITICAL)


here=os.path.split(__file__)[0]
Config={}


def get_weather(query) :
    url=Config["openweathermap"]["endpoint"]
    myquery=copy.deepcopy(query)
    myquery.update({"appid" : Config["openweathermap"]["apikey"]})
    response=requests.get(url,params=myquery)
    response.raise_for_status()
    logging.debug("Query {} - Response {} ".format(query,response.content))
    return response.json()




def load_config(filename=None) :
    global Config
    if filename is None :
        filename=os.path.join(here,"config.json")
    try :
        Config=json.load(open(filename))
    except Exception as e :
        raise ValueError("Loading configuration file {} failed: {} ".format(filename,repr(e)))



def set_root_logger(loglevel) :
    try :
        if type(loglevel)==int :
            level=loglevel
        else :
            level=getattr(logging,loglevel)
            assert(type(level)==int)
        logging.getLogger().setLevel(level)
    except Exception as e:
        raise ValueError("Failed setting log level to {}: {}".format(loglevel,repr(e)))


def fetch_json(url,payload=None,method=None,**kwargs) :
    ## kwargs={}
    if payload is None and ("data" not in kwargs) :
        method="GET"
    else :
        if method is None :
            method="POST"
        if payload is not None :
            kwargs["json"]=payload
    kwargs["headers"]={ "Content-Type" : "application/json" }
    logging.debug("{method} {url} payload={payload} data={}".format(kwargs.get("data",None), **locals()))
    resp=requests.request(method,url,**kwargs)
    resp.raise_for_status()
    try :
        return resp.json()
    except Exception as e:
        return resp


create_ae     = """{"m2m:ae":  {"rn": "%(rn)s", "rr": false, "api": ""}}"""
create_device = """{"m2m:cnt": {"rn": "%(rn)s"}}"""
create_data   = """{"m2m:cin": { "con" : "%(data)s" }}"""



@lru_cache(maxsize=1000)
def ensure_sensor(ae,device,sensor) :
    ep=Config["openmtc"]["endpoint"]
    for (p,n,jp) in ( ("{}".format(ep),ae,create_ae % dict(rn=ae)),
                    ("{}/{}".format(ep,ae),device,create_device % dict(rn=device)),
                    ("{}/{}/{}".format(ep,ae,device),sensor,create_device % dict(rn=sensor))) :
        try :
            r=fetch_json("{}/{}".format(p,n))
        except requests.HTTPError :
            r=fetch_json(p,payload=json.loads(jp % dict(rn=n)))
            logging.debug("Created: {n} in {p}".format(**locals()))

@lru_cache(maxsize=1000)
def ensure_entity(name) :
    ep=Config["orion"]["endpoint"]
    try :
        r=fetch_json(ep,data='{' +'"id" : "{}", "type" : "{}"'.format(name,Config["orion"]["entitytype"])+'}',method="POST")
    except requests.HTTPError as e:
        logging.debug("Creation of {} failed: {}".format(name,e))
    else :
        logging.debug("Created: {} - {} ".format(name,r))




def update_openmtc(ae,device,sensor,datum) :
    ep=Config["openmtc"]["endpoint"]
    ensure_sensor(ae,device,sensor)
    path="{ep}/{ae}/{device}/{sensor}".format(**locals())
    payload=json.loads(create_data % dict(data=datum,rn="data"))
    r=fetch_json(path, payload=payload,method="POST")
    logging.debug("{path} stored".format(**locals()))


def extract(source,data) :
    if "{" in source :
        jsp=jmespathtemplate(source)
        datum=jsp.render(data)
    else :
        jsp=jmespath.compile(source)
        datum=jsp.search(data)
    return datum


def store_openmtc(data) :
    global Config
    device=extract(Config["openmtc"]["devicename"],data)
    sensors=Config["openweathermap"].get("sensors",{})
    sensors.update(Config["openmtc"].get("sensors",{}))
    for (source,sensor) in sensors.items() :
        try :
            datum=extract(source,data)
            update_openmtc(Config["openmtc"]["ae"],device,sensor,datum)
            if datum is None :
                logger.warning("{sensor} from {source} is None".format(**locals()))
        except Exception as e :
            logger.error("Updating {sensor} with {datum} failed: {e}".format(**locals()))


def store_orion(data) :
    global Config
    entity=extract(Config["orion"]["entityname"],data)
    ep="{}/{}/attrs".format(Config["orion"]["endpoint"],entity)
    ensure_entity(entity)
    sensors=Config["orion"].get("sensors",{})
    update={}
    for (source,sensor) in sensors.items() :
        datum=extract(source,data)
        update[sensor["name"]]=dict(value=datum,type=sensor["type"])
    try :
        r=fetch_json(ep,data=json.dumps(update),method="PUT")
    except requests.HTTPError as e :
        logger.error("Orion update with {} failed : {}".format(update,e))
    else :
        logger.info("Orion updated with {}: {} ".format(update,r))


def store_geojson(alldata) :
    global Config
    from geojson import Point, Feature, FeatureCollection, dump
    features=[]
    sensors=Config["geojson"].get("sensors",{})
    for data in alldata :
        properties={}
        for (source,sensor) in sensors.items() :
            properties[sensor]=extract(source,data)
        features.append(Feature(geometry=Point([data["coord"]["lon"],data["coord"]["lat"]]),properties=properties))
    c=FeatureCollection(features)
    with open(Config["geojson"]["path"],"w") as f :
        dump(c,f)
    logger.info("Written {} points to GeoJSON {}".format(len(features),Config["geojson"]["path"]))


def store_csv(alldata) :
    global Config
    places=[]
    sensors=Config["csv"].get("sensors",{})
    for data in alldata :
        properties=OrderedDict()
        for (source,sensor) in sensors.items() :
            properties[sensor]=extract(source,data)
        places.append(properties)
    with open(Config["csv"]["path"],"w") as f :
        writer=csv.DictWriter(f,fieldnames=places[0].keys())
        writer.writeheader()
        writer.writerows(places)
    logger.info("Written {} points to CSV {}".format(len(places),Config["csv"]["path"]))


def mqtt_client(url,**kwargs) :
    parsed=urllib.parse.urlparse(url)
    host=parsed.netloc.split(":")
    if len(host)==1 :
        host.append('1883')
    if parsed.scheme in ('tls','websockets') :
        kwargs["transport"]=parsed.scheme
    import paho.mqtt.client
    client=paho.mqtt.client.Client(**kwargs)
    client.connect(host[0],int(host[1]))
    logger.debug("Connected to {host[0]} [{kwargs}]".format(**locals()))
    client.loop_start()
    return client

def store_mqtt(data,client) :
    global Config
    sensors=Config["mqtt"].get("sensors",{})
    topictemplate=jmespathtemplate(Config["mqtt"]["topic"])
    topicdata=copy.deepcopy(data)
    for (source,sensor) in sensors.items() :
        datum=extract(source,data)
        topicdata.update(dict(sensor=sensor))
        topic=topictemplate.render(topicdata)
        rv=client.publish(topic,datum,qos=Config["mqtt"]["qos"])
        logger.debug("published to {topic}: {datum} {rv}".format(**locals()))


protocols={
            'MQTTv31'  : paho.mqtt.client.MQTTv31,
            'MQTTv311' : paho.mqtt.client.MQTTv311
          }


@click.command()
@click.option('--loglevel',default="INFO",
                           help="Python loglevel, one of DEBUG,INFO,WARNING,ERROR,CRITICAL")
@click.option('--config',default=os.path.join(here,"config.json"),
                           help="JSON config file")
@click.option('--once/--loop',is_flag=True,
                           help="run just once (no loop)")
@click.option('--nostop/--stop',is_flag=True,
                           help="do not stop on error")
def run_update(loglevel,config,once,nostop) :
    global Config
    global ensure_sensor
    set_root_logger(loglevel)
    load_config(config)
    for (source,function) in (("openmtc",ensure_sensor), ("orion",ensure_entity)) :
        if source in Config :
            if "cache" in Config[source] :
                if Config[source]["cache"] == 0 :
                    # uncached
                    globals()[function.__name__]=function.__wrapped__
                else :
                    globals()[function.__name__]=lru_cache(maxsize=Config[source]["cache"])(function.__wrapped__)
    if "geojson" in Config and Config["geojson"].get("enabled",False) :
        try :
            from geojson import Point, FeatureCollection
        except ImportError :
            logger.error("Please install python GeoJSON module to enable GeoJSON export.")
            Config["geojson"]["enabled"]=Fals
    if "mqtt" in Config and Config["mqtt"].get("enabled",False) :
        try :
            import paho.mqtt
        except ImportError :
            logger.error("Please install paho.mqtt module to enable Mqtt export.")
            Config["mqtt"]["enabled"]=Fals
    while True :
        try :
                alldata=[]
                if "mqtt" in Config and Config["mqtt"].get("enabled",False) :
                    args=dict()
                    for pn in ("client_id", "clean_session", "userdata", "protocol", "transport") :
                        if pn in Config["mqtt"] :
                            if pn == 'protocol' :
                                try :
                                    args[pn]=protocols[Config["mqtt"][pn]]
                                except KeyError :
                                    raise ValueError("Wrong protocol parameter for mqtt: {}. Available protocols: {}".format(Config["mqtt"][pn],",".join(protocols.keys())))
                            else :
                                args[pn]=Config["mqtt"][pn]
                    mqtt=mqtt_client(Config["mqtt"]["endpoint"],**args)
                else :
                    mqtt=None
                for location in Config["locations"] :
                    logger.info("Getting {}".format(location.get("name",repr(location["query"]))))
                    data=get_weather(location["query"])
                    data["location"]=location
                    if "openmtc" in Config and Config["openmtc"].get("enabled",False) :
                        store_openmtc(data)
                    if "orion"   in Config and Config["orion"].get("enabled",False) :
                        store_orion(data)
                    if mqtt is not None :
                        store_mqtt(data,mqtt)
                    alldata.append(data)
                if mqtt is not None :
                    mqtt.disconnect()
                if "geojson" in Config and Config["geojson"].get("enabled",False) :
                    store_geojson(alldata)
                if "csv" in Config and Config["csv"].get("enabled",False) :
                    store_csv(alldata)
                if once :
                    break
        except Exception as e:
            if not nostop :
                raise
            else :
                logger.exception(e)
        else :
            pass
        logging.info("Sleeping for {} seconds".format(Config["refresh"]))
        time.sleep(Config["refresh"])











if __name__=="__main__" :
    run_update()
