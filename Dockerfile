FROM frolvlad/alpine-python3
MAINTAINER mv@datenfreunde.com

LABEL Description="distribute openweathermap data to several data stores or file formats" 

WORKDIR /app
COPY . /app
Volume /app/openweathermap-html


RUN pip install --upgrade pip && \
    pip install -r requirements.txt
ENTRYPOINT ["python3", "openweathermap.py"]
CMD ["--help"]
