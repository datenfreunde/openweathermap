# openweathermap component 

![](screenshot.png "Screenshot of opeweathermap HTML viewer")

This service is part of the [SmartOrchestra](http://smartorchestra.de) project funded by the [Smart Service Initiative](https://www.bmwi.de/DE/Themen/Digitale-Welt/Digitale-Technologien/smart-service-welt.html) of the Federal Ministery for Economic Affairs and Energy.
It gives you a configurable smart service component: a sensor data source with an attached freemium business model.

This program will update a configured number of data targets with current weather data from configured locations in configured intervals:

  - OpenMTC
  - FIWARE Orion Context Broker
  - MQTT
  - CSV
  - GEOJSON
  - HTML
  
It will create the configured Application Entity and Device in OpenMTC if they don't exist already. Likewise, it will create Entities and Properties in the FIWARE Orion Context Broker if they don't exist, and topics in MQTT. All these aspects can be configured. A Dockerfile is included for convenience.

A running example can be seen at [apps.opendatacity.de/openweathermap/](https://apps.opendatacity.de/openweathermap/). It shows current temperatures at the location of the six project partners of SmartOrchestra.

# Running in Docker

A Dockerfile is included. You can run this application inside a docker container, providing the configuration file at startup, like so:

```bash
docker built -t openweathermap . && docker run --rm -v /path/to/config.json:/config.json --name openweathermap  openweathermap --config=/config.json
```

Of course, to write data to services in other containers, you have to enable network traffic between them. If your Mosquitto server was running in a container named "mosquitto",you can do for example:

```bash
 run --rm -v /path/to/config.json:/config.json --link mosquitto:mosquitto --name openweathermap  openweathermap  --config=/config.json
```

Inside the `config.json`, you have to set `tcp://mosquitto` as the endpoint.

Please see the [Docker docs](https://docs.docker.com/engine/tutorials/networkingcontainers/) for details.

## Installation

If you want to run this application outside of Docker, you will need *python 3*. Please follow these steps:

--- 1. Install requirements

```bash

pip3 install -r requirements.txt

```

--- 2. Copy `config.json.dist` to `config.json` and insert values for openweathermap api (you can get it from openweathermap.org) and OpenMTC endpoint (you can get those from anybody running a reachable openMTC instance, i.e. Fraunhofer Fokus). 

See example `config.json.dist` below.

Therein, you'll find (among other things) these three sections: 

  - locations (weather data is fetched for these locations)
  - openweathermap (endpoint and other details about the source of the data) 
  - openmtc,orion,csv, geojson (endpoints and other details for writing the data to targets)

Please see section "How to configure" below on details about the JSON file. 

The example file below would write the current temperature at the openweathermap station Moabit (id 2870310) - the one that is closest to Fraunhofer Fokus' offices at 52.52N, 13.31W - to openMTC as
sensor data at the url `http://(endpoint name)/onem2m/openweathermap/2870310/temperature`. The temperature would be updated every 3600 seconds.


### Example config.json
```json

{
    "locations": [
        {
            "name": "Fraunhofer Fokus",
            "query": {
                "lat": 52.526134,
                "lon": 13.314487
            }
        }
    ],
    "openmtc": {
        "enabled" : true,
        "ae": "openweathermap",
        "cache": 1000,
        "devicename": "{id}",
        "endpoint": "---get-from-fokus.fraunhofer.de",
        "sensors": {
            "main.temp": "temperature",
        }
    },
    "orion": {
        "cache": 1000,
        "enabled": true,
        "endpoint": "--please-run-your-own",
        "entityname": "{id}",
        "entitytype": "openweathermap",
        "sensors": {
            "main.humidity": {
                "name": "humidity",
                "type": "Ingeger"
            },
    "openweathermap": {
        "apikey": "---get---from---http://openweathermap.org/api",
        "endpoint": "http://api.openweathermap.org/data/2.5/weather"
    },
    "refresh": 3600
}
```


--- 3. Run

```
python openweathermap.py --config=config.json --loglevel=DEBUG
```


## How to Configure

Configuration happens in a JSON file, see above or `config.json.dist` in this repository for a full example. Top-level attributes of the file are: 

**refresh** configures the number of seconds between updates of the configured data targets with fresh weather data.


**openweathermap** configures access to the openweathermap data and the mapping between this data and sensor names in openmtc. 

#### Example openweathermap configuration snippet
```json
    "openweathermap": {
        "apikey": "---get---from---http://openweathermap.org/api",
        "endpoint": "http://api.openweathermap.org/data/2.5/weather"
    },
```


**openmtc** configures the openmtc endpoint and some aspects of where exactly to store openwheathermap data inside the openMTC instance, such as the top level `ae` ("application entity") and the `devicename`. 
The property "enabled" has to be set to a truthy value, otherwise, no data will be written to the openmtc endpoint. 
The `devicename` to be used for the data in OpenMTC is a [jmespath](http://jmespath.org) template, a string that can contain jmespath expressions enclosed in `{` and `}`, and is rendered with the openweathermap object as input.
In that way, using "{id}" as a value, devices will be named after the station
ID returned by openweathermap, e.g. `2870310` for Moabit. 
If you want it more human readable, you could use "{id}-{name}" as a value in the configuration instead of `{id}`. The device would then be named `2870310-Moabit` instead of just `2870310`.
The attribute `cache` refers to the cache size. The cache stores knwoledge about the existence of sensors within the application entity in the current OpenMTC instance. 
The application follows a "check for existence of device, and create it if it does not exist, before write" approach. This check is skipped for devices that have existed in the recent past. If the cache size is 1000, the existence of the last 1000 accessed sensors is rememberd locally. No attempt to check existence and/or create these
objects before writing the sensor data will be made. Cache size of 0 will disable this feature. This will put more load on the OpenMTC instance and increase network traffic.

#### Example openmtc configuration snippet
```json

    "openmtc": {
        "enabled" : true,
        "ae": "openweathermap",
        "cache": 1000,
        "devicename": "{id}",
        "endpoint": "---get-from-fokus.fraunhofer.de"
        "sensors": {
            "main.temp": "temperature"
        }
    }
```

Inside `openmtc`, the *sensors* attribute configures a mapping. Keys are the the data source attribute (defined by a [JMESPath](http://jmespath.org) in the JSON returned by openweathermap, or by a jmespath template, a string with jmespath expressions enclosed in `{` and `}`). The value of the mapping represents
the destination sensor name in OpenMTC. 

#### Example sensor configuration
```json
        "sensors": {
            "main.temp": "temperature",
        }
```

The example above would take the value of attribute `temp` inside the `main` object of the openweathermap JSON and write it to a sensor name `temperature`. See below for an example openweathermap JSON.

**mqtt** is a set of configurations for publishing the sensor data to a MQTT message broker like [Mosquitto](https://mosquitto.org/). The topic names
are inferred from the sensor name and metadata from openweathermap, most commonly {id} and {name}. For the example below, the value `main.temp` for station `334355` would be published under the topic `/openweathermap/334335/temperature`. 

For the MQTT-specific configuration parameters like `qos`, `protocol`, `client_id` and `clean_session` please see the documentation of [the underlying Python implementation paho-mqtt](https://eclipse.org/paho/clients/python/docs/#client).

#### Example mqtt configuration snippet
```json
	"mqtt":        {
                         "enabled"    : true,
						 "endpoint"   : "tcp://localhost:1883",
                         "protocol"   : "MQTTv31",
                         "client_id"  : "openweathermap-client",
                         "qos"        : 0,
                         "topic"      : "/openweathermap/{id}/{sensor}",
                         "sensors" : {
                             "main.pressure"    : "pressure",
                             "main.humidity"    : "humidity",
                             "main.temp"        : "temperature"
                         }
					  }
```



**orion** is a set of configurations for feeding data to the [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/), an NSGI9/10-compliant data broker with a REST API that supports subscription /notification for data updates, geolocation searches and other nice features, and which is part of the EU-funded [FiWARE](https://www.fiware.org/) project.

The configuration follows the same logic as the one for OpenMTC - see the example below. Orion supports typed sensor data, and devices are called "entities" - that's the main differences as to how data is stored. 

You can use jmespath templates for complex values - the `position` property below is composed of `coord.lon` and `coord.lat`, as it should be per the [Orion documentation for geolocation](https://fiware-orion.readthedocs.io/en/master/user/geolocation/index.html).

```json
    "orion": {
        "enabled": true,
        "cache" : 1000,
        "endpoint": "--please-run-your-own",
        "entityname": "{id}",
        "entitytype": "openweathermap",
        "sensors": {
            "main.humidity": {
                "name": "humidity",
                "type": "Ingeger"
            },
            "{coord.lat}, {coord.lon}"  : { 
                "name" : "position", 
                "type" : "geo:point" 
            }

        }
```


**locations** are configured with a mapping of the query parameters as defined on the [openweathermap api documentation](http://openweathermap.org/current). Those can be city names, latitude/longitude values or station IDs. The attribute "name" is optional and is useful for reference.


#### Example location configuration
```json
"locations" : [
        {
            "name": "Fraunhofer Fokus",
            "query": {
                "lat": 52.526134,
                "lon": 13.314487
            }
        }
]
```
### Example openweathermap current weather JSON response.


Below is a sample JSON response for the current weather. Please also see the [documentation on openweathermap.org](http://openweathermap.org/current). 


```JSON
{
  "coord": {
    "lon": 10,
    "lat": 53.55
  },
  "weather": [
    {
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10n"
    },
    {
      "id": 310,
      "main": "Drizzle",
      "description": "light intensity drizzle rain",
      "icon": "09n"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 276.15,
    "pressure": 1006,
    "humidity": 93,
    "temp_min": 276.15,
    "temp_max": 276.15
  },
  "visibility": 10000,
  "wind": {
    "speed": 3.1,
    "deg": 30
  },
  "clouds": {
    "all": 90
  },
  "dt": 1478499600,
  "sys": {
    "type": 1,
    "id": 4883,
    "message": 0.0336,
    "country": "DE",
    "sunrise": 1478500314,
    "sunset": 1478532886
  },
  "id": 2911298,
  "name": "Hamburg",
  "cod": 200
}
```


### Viewing the data

The repository contains a minimal HTML app to view the data (if the configured openweathermap stations are in or near Germany) in the directory `openweathermap-html`. The file `config.json.dist` contains an example of how to configure the `csv` property in the configuration file to update the HTML app. You can see the HTML app working at [//apps.opendatacity.de/openweathermap/](https://apps.opendatacity.de/openweathermap/). 


### Other Export Formats

**GeoJSON** files will contain one point per weather station. The properties of the point will contain the sensor data.

#### Example geojson configuration snippet
```JSON
"geojson":        {
                 "enabled"    : true,
                 "path"       : "openweathermap-html/openweathermap.geo.json",
                 "sensors" : {
                     "name"                                 : "name",
                     "main.temp"                            : "temperature",
                     "sys.country"                          : "country",
                     "{weather[0].main}, {weather[1].main}" : "weather"
                 }
}
```

The result will look like this:

```JSON
{
  "type": "FeatureCollection"
  "features": [
    {
      "geometry": {
        "coordinates": [
          13.2,
          52.64
        ],
        "type": "Point"
      },
      "properties": {
        "temperature": 267.31,
        "name": "Hennigsdorf",
        "country": "DE",
        "weather": "Clear, None"
      },
      "type": "Feature"
    }
  ]
}
```


**csv** files will contain one line of output per weather station into the configured csv file.
The first line of the csv file will containe the field names.

#### Example csv configuration snippet
```JSON
"csv":        {
					 "enabled"    : true,
					 "path"       : "openweathermap-html/openweathermap.csv",
					 "sensors" : {
						 "name"                                 : "name",
						 "main.temp"                            : "temperature",
						 "sys.country"                          : "country",
						 "{weather[0].main}, {weather[0].description}" : "weather",
						 "coord.lat"                            : "lat",
						 "coord.lon"                            : "lon",
						 "id"                                   : "id"
					 }
				  }
```

The resulting CSV file will look something like this:

```
name,lat,weather,temperature,country,lon
Moabit,52.53,mist,272.15,DE,13.34
"Aachen, Stadt",50.78,mist,278.4,DE,6.09
```

### Viewing the results in HTML

todo (see `openweathermap-html/` for a start)


## Dependencies

Python dependencies are listed in requirements.txt. To generate the outlines for the HTML view from scratch, you need [topojson](https://github.com/topojson/topojson). The Makefile depends on GNU Make. 

(c) 2016-2017 Datenfreunde GmbH
