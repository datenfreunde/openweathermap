
SHELL := /bin/bash

.PHONY: push-config orion mosquitto 



config.json.dist : config.json
	python -c 'import json,sys;from collections import OrderedDict; j=json.load(sys.stdin,object_pairs_hook=OrderedDict);j["openmtc"]["endpoint"]="---get-from-fokus.fraunhofer.de";j["openweathermap"]["apikey"]="---get---from---http://openweathermap.org/api";j["orion"]["endpoint"]="--please-run-your-own";j["mqtt"]["endpoint"]=j["orion"]["endpoint"];json.dump(j,sys.stdout,indent=4)' <config.json >config.json.dist

push-config : config.json
	rsync  -av config.json nyx:/home/mvirtel/projekte/openweathermap

mosquitto :
	docker run --rm --name mosquitto -ti -p 1883:1883 -p 9001:9001 \
	toke/mosquitto

orion :
	cd ../fiware-orion && docker-compose up

test-docker: 
	 docker run --rm -v /home/martin/projekte/smartorchestra/openweathermap/config.json:/config.json --link mosquitto:mosquitto --link fiwareorion_orion_1:orion --name openweathermap openweathermap --config=/config.json



docker-build : SHELL:=/bin/bash
docker-build :
	docker build -t $$(basename "$$PWD") . 
	
