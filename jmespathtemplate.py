import jmespath
import json
import re
from collections import namedtuple


replace=namedtuple('replace',['start','end','path'])

regex=re.compile("\{(.*?)\}")

class jmespathtemplate(object) :
    """
    Simple template class.

    Expressions in { } will be converted to jmespath. When rendering the template
    against a data structure, they will be replaced with the result of the path expression.

    a=jmespathtemplate("{a.b[0]}")
    print(a.render({ "a" : { "b" : ["first","second"] } }))
    >> first
    """

    __slots__=['found','original']


    def __init__(self,val) :
        self.original=val
        self.found=[replace(a.start(),a.end(),jmespath.compile(a.groups()[0])) for a in regex.finditer(val)]
        self.found.reverse()


    def render(self,data):
        if type(data) == str :
            data=json.loads(data)
        c=self.original
        for r in self.found :
            c=c[0:r.start]+str(r.path.search(data))+c[r.end:]
        return c




if __name__=='__main__' :
    g='{ "a" : { "b" : 1 }, "c" : 2 }'
    t=jmespathtemplate("{a.b} {a} {c}")
    assert t.render(g) == "1 {'b': 1} 2"
    a=jmespathtemplate("{a.b[0]}")
    assert a.render({ "a" : { "b" : ["first","second"] } }) == "first"



